from django.urls import path, include
from apps.user_account import views
from django.contrib.auth import views as auth_views

app_name = 'user_account'


reset_change_urls = [
    path('password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('pass_reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('pass_reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]

urlpatterns = [
    path('signup/', views.SignupView.as_view(), name='signup'),
    path('account-activate/<uid>/<token>/', views.AccountActivate.as_view(), name='activate'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('home/', views.Home.as_view(), name='home'),
    path('test-onclick/', views.test.as_view(), name='test'),
    path('user/', include(reset_change_urls)),
]

