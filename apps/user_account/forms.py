from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django import forms
from django.contrib.auth.forms import AuthenticationForm

from apps.user_account.ajax_mixin import DictErrorMixin
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox, ReCaptchaV2Invisible, ReCaptchaV3


class CustomUserCreation(DictErrorMixin, UserCreationForm):
    # captcha = ReCaptchaField()

    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2', 'avatar',
                  # 'dob', 'gender', 'captcha'
                  ]
        labels = {
            'password1': _('Password'),
            'password2': _('Password Again')
        }
        widgets = {
            'gender': forms.RadioSelect
        }


class LoginForm(DictErrorMixin, AuthenticationForm):
    pass
