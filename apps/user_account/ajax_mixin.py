from django import forms
from django.template.defaultfilters import striptags
from django.utils.translation import gettext as _
from django.http import JsonResponse


class DictErrorMixin(forms.BaseForm):
    def dict_errors(self, strip_tags=True):
        errors = {}
        flag = False
        if self.prefix:
            self.prefix += "-"
        for error in self.errors.items():
            prefix_key = self.prefix if self.prefix else ""
            errors[prefix_key + error[0]] = _(
                striptags(error[1]) if strip_tags else error[1]
            )
        return errors


class AjaxResponseMixin(object):
    def form_invalid(self, form):
        super().form_invalid(form)
        data = {
            'errors': form.dict_errors()
        }
        return JsonResponse(data, status=400)

    def form_valid(self, form):
        super().form_valid(form)
        data = {
            'message': self.get_success_message(),
            'redirect_url': self.get_success_url()
        }
        return JsonResponse(data, status=200)

    def get_success_message(self):
        return self.success_message
