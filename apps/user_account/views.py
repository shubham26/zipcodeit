from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import views as auth_views
from django.views import generic
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.translation import gettext_lazy as _
from django.shortcuts import render
from django.conf import settings

from apps.user_account.forms import LoginForm
from apps.user_account.ajax_mixin import AjaxResponseMixin
from apps.user_account.forms import CustomUserCreation


class SignupMixin(object):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        response = HttpResponseRedirect(self.get_success_url())
        current_site = get_current_site(self.request)
        email_subject = 'Activate Your ZIPCODEIT Account'
        token_generator = PasswordResetTokenGenerator()
        message = render_to_string('account_activation_email.html', {
            'user': self.object,
            'request': self.request,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(self.object.pk)).decode(),
            'token': token_generator.make_token(self.object),
        })
        self.object.email_user(email_subject, message, getattr(settings, 'EMAIL_HOST_USER', None))
        return response

    def form_invalid(self, form):
        print('Form Invalid')
        return super().form_invalid(form)


class SignupView(AjaxResponseMixin, SignupMixin, generic.CreateView):
    model = get_user_model()
    template_name = 'login.html'
    form_class = CustomUserCreation
    success_url = reverse_lazy('user_account:login')
    form_context_name = 'signup_form'
    success_messge = _('Signup Successful')

    def get_context_data(self, **kwargs):
        if self.form_context_name not in kwargs:
            kwargs[self.form_context_name] = self.get_form()
        return super().get_context_data(**kwargs)


class AccountActivate(generic.TemplateView):
    template_name = 'home.html'
    error_message = _('This link is invalid or this link has been used earlier')
    success_message = _('Your account has been activated successfully')

    def get(self, request, *args, **kwargs):
        uid = kwargs.get('uid')
        token = kwargs.get('token')
        try:
            user_pk = urlsafe_base64_decode(uid).decode()
        except (TypeError, ValueError, OverflowError):
            user_pk = None
        user_class = get_user_model()
        user = user_class.objects.filter(pk=user_pk)
        token_generator = PasswordResetTokenGenerator()
        context = {'message': self.error_message}
        if user.exists():
            if token_generator.check_token(user[0], token):
                user[0].is_active = True
                user[0].save()
                context = {
                    'activation_message': self.success_message
                }
        return render(request, self.template_name, context)


class LoginView(AjaxResponseMixin, auth_views.LoginView):
    template_name = 'login.html'
    success_url = reverse_lazy('user_account:home')
    form_context_name = 'login_form'
    form_class = LoginForm
    success_message = _('Login Successful')

    def get_context_data(self, **kwargs):
        if self.form_context_name not in kwargs:
            kwargs[self.form_context_name] = self.get_form()
        return super().get_context_data(**kwargs)


class Home(generic.TemplateView):
    template_name = 'home.html'


class test(generic.TemplateView):
    template_name = 'test.html'
