from django.shortcuts import render
from django.views import generic
from django.http.response import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.db.models import F, Q
from django.contrib.gis.geos import GEOSGeometry, Point
from django.http import JsonResponse

from apps.business_account import models
from apps.business_account import forms as business_forms


standard_json_data = {'result': True, 'errors': {}, 'message': None}

initial_qs_limit = 20


class CreateBusinessView(generic.CreateView):
    model = models.BusinessAccount
    template_name = 'simple_form.html'
    form_class = business_forms.BusinessFormTest
    extra_context = {'form_url': reverse_lazy('business:business_create')}

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        # return HttpResponseRedirect(self.get_success_url())
        return JsonResponse(data=None, status=200)

    def form_invalid(self, form):
        response = super().form_invalid(form)
        return JsonResponse(data=form.errors, status=200)


def check_form_1(request):
    if request.method != 'POST':
        return JsonResponse(data=None, status=400)
    business_form = business_forms.BusinessForm(request.POST)
    address_form = business_forms.AddressForm(request.POST)

    if business_form.is_valid() and address_form.is_valid():
        data = {'result': True, 'message': 'form valid'}
        return JsonResponse(data=data, status=200)
    else:
        errors = {}
        if business_form.errors:
            errors.update(business_form.errors)
        if address_form.errors:
            errors = {**errors, **address_form.errors}
        #     make true for test under result
        data = {'errors': errors, 'result': False}
        return JsonResponse(data=data, status=200)


def check_form_2(request):
    if request.method != 'POST':
        return JsonResponse(data=None, status=400)
    data = {'result': True, 'message': 'form valid', 'errors': dict()}
    payment_form = business_forms.PaymentDetailsForm(request.POST)
    address_check = request.POST.get('checkbox')
    if not address_check:
        address_form = business_forms.SecondAddressForm(request.POST)
        if address_form.is_valid():
            data = {'result': True, 'message': 'form valid', 'errors': dict()}
        else:
            data = {'result': False, 'errors': address_form.errors, 'message': 'form invalid'}
    if payment_form.is_valid():
        data['result'] = data['result'] and True
    else:
        #     make true for test under result
        data['result'] = False
        data['errors'] = {**data['errors'], **payment_form.errors}
        data['message'] = 'form invalid'
    return JsonResponse(data=data, status=200)


def submit_form_3(request):
    if request.method != 'POST':
        return JsonResponse(data=None, status=400)
    address_check = request.POST.get('checkbox')
    forms = dict()
    forms['business_form'] = business_forms.BusinessForm(request.POST)
    forms['address_form'] = business_forms.AddressForm(request.POST)
    forms['payment_form'] = business_forms.PaymentDetailsForm(request.POST)
    forms['costumer_form'] = business_forms.CostumerTypeForm(request.POST)
    if not address_check:
        forms['related_address_form'] = business_forms.SecondAddressForm(request.POST)

    data = standard_json_data.copy()

    for form in forms.values():
        data['result'] = data['result'] and bool(form.is_valid())
        data['errors'] = {**data['errors'], **form.errors}

    print(data['errors'].keys())
    if data['result'] and bool(request.user):
        try:
            address_obj = forms['address_form'].save(commit=False)
            address_obj.user = request.user
            address_obj.save()

            business_obj = forms['business_form'].save(commit=False)
            business_obj.user = request.user
            business_obj.address = address_obj
            business_obj.save()

            payment_address = address_obj
            if not address_check:
                payment_address = forms['related_address_form'].save()

            payment_obj = forms['payment_form'].save(commit=False)
            payment_obj.business = business_obj
            payment_obj.billing_address = payment_address
            payment_obj.save()

            costumer_obj = forms['costumer_form'].save(commit=False)
            costumer_obj.business = business_obj
            costumer_obj.save()

        except KeyError:
            try:
                business_obj.delete()
            except models.BusinessAccount.DoesNotExist:
                pass
    return JsonResponse(data=data, status=200)


class PaymentDetailsCreateView(generic.CreateView):
    model = models.PaymentsDetails
    template_name = 'simple_form.html'


def test(request):
    pos = request.POST.get('position')
    print(pos)


class IndexTest(generic.TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # todo fix recommended and top rated on leter
        context['recommended'] = models.IndustryType.objects.all()
        context['top_rated'] = models.BusinessAccount.objects.all()
        return context


class GridView(generic.ListView):
    template_name = 'old/resources-grid.html'
    model = models.BusinessAccount
    context_object_name = 'list_qs'
    # paginate_by = 2

    args_list = ['business_name', 'business_type', 'industry_type', 'industry_category']

    def get_query_params(self):
        filter_kwargs = {}
        get_kwargs = self.request.GET

        if 'business_name' in get_kwargs:
            filter_kwargs['business_name__lower'] = get_kwargs.get('business_name')
        if 'business_type' in get_kwargs:
            filter_kwargs['business_type__lower'] = get_kwargs.get('business_type')
        if 'industry_type' in get_kwargs:
            filter_kwargs['industry_type__type'] = get_kwargs.get('industry_type')
        if 'industry_category' in get_kwargs:
            filter_kwargs['industry_type__category__category__lower'] = get_kwargs.get('industry_category')
        return filter_kwargs

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset(*args, **kwargs)
        # client_ip = self.request.META.get('REMOTE_ADDR')
        # client_ip = '216.58.221.46'
        # client_coord = GeoIP2().coords(client_ip)

        filter_kwargs = self.get_query_params()

        qs = qs.annotate(live_distance=Distance('address__geo_loc', Point([0.000000,0.000000], srid=4326))). \
            annotate(business_location=F('address__geo_loc'))
        qs = qs.filter(**filter_kwargs)

        return qs


class BusinessDetailView(generic.DetailView):
    model = models.BusinessAccount
    template_name = 'choice.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # todo set user location in map,
        # todo give live distance
        # context['live_distance'] = self.object.address.geo_loc.
        return context


class MapDirectionView(generic.DetailView):
    model = models.BusinessAccount
    template_name = 'choice-option-details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # todo set user location in map,
        # todo give live distance
        # context['live_distance'] = self.object.address.geo_loc.
        context['recom_list'] = models.BusinessAccount.objects.all()
        return context
