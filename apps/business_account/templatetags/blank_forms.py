from django import template

from apps.business_account import forms

register = template.Library()


@register.simple_tag
def get_business_form():
    return forms.BusinessForm()


@register.simple_tag
def get_address_form():
    return forms.AddressForm()


@register.simple_tag
def get_payment_form():
    return forms.PaymentDetailsForm()


# @register.simple_tag
# def get_blank_form():
#     return {'business_form': forms.BusinessForm(),
#             'address_form': forms.AddressForm(),
#             'payment_form': forms.PaymentDetailsForm(),
#             'second_address_form': forms.SecondAddressForm(),
#             }
#

@register.simple_tag
def get_form_fields():
    return list(forms.BusinessForm().fields.keys()) + \
            list(forms.AddressForm().fields.keys()) + \
            list(forms.PaymentDetailsForm().fields.keys()) + \
            list(forms.SecondAddressForm().fields.keys()) + \
            list(forms.CostumerTypeForm().fields.keys())


