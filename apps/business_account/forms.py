from django import forms

from apps.business_account import models


class BusinessFormTest(forms.ModelForm):
    # position = forms.CharField(widget=)

    class Meta:
        model = models.BusinessAccount
        exclude = ['user', 'address']
        # widgets = {
        #
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for field in self.fields:
        #     field


class BusinessForm(forms.ModelForm):

    class Meta:
        model = models.BusinessAccount
        exclude = ['user', 'address']


class AddressForm(forms.ModelForm):

    class Meta:
        model = models.Address
        exclude = ['user', 'geo_loc']


class SecondAddressForm(forms.ModelForm):

    class Meta:
        model = models.Address
        exclude = ['user', 'geo_loc']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        local_fields = self.fields.copy()

        for key, val in local_fields.items():
            self.fields.pop(key)
            self.fields[key+'_second'] = val


class PaymentDetailsForm(forms.ModelForm):

    class Meta:
        model = models.PaymentsDetails
        exclude = ['business', 'billing_address']


class CostumerTypeForm(forms.ModelForm):

    class Meta:
        model = models.CostumerType
        exclude = ['business']



