from apps.business_account.models import IndustryCategory, BusinessType
from django.conf import settings


def industry_category(request):
    return {'industries': IndustryCategory.objects.all()}


def business_type(request):
    return {'businesses': BusinessType.objects.all()}


def map_api_key(request):
    return {'map_api_key': getattr(settings, 'GOOGLE_MAP_API')}
