from django.contrib import admin
from django.urls import path, include
from apps.business_account import views

app_name = 'business'

grid_view_urlpatterns = [
    path('/<industry>/', views.GridView.as_view(), name='grid_view'),

]

urlpatterns = [
    # path('business-create/', views.CreateBusinessView.as_view(), name='business_create'),
    path('business-check-1/', views.check_form_1, name='check_form_1'),
    path('business-check-2/', views.check_form_2, name='check_form_2'),
    path('business-check-3/', views.submit_form_3, name='submit_form_3'),
    # path('test/', views.test, name='test'),
    path('', views.IndexTest.as_view(), name='index'),
    path('grid-view/', views.GridView.as_view(), name='grid_view'),
    path('detail-view/<int:pk>/', views.BusinessDetailView.as_view(), name='detail_view'),
    path('direction-view/<int:pk>/', views.MapDirectionView.as_view(), name='direction_view'),
]
