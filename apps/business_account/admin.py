from django.contrib.gis import admin
from apps.business_account import models

admin.site.register(models.IndustryCategory)
admin.site.register(models.IndustryType)
admin.site.register(models.BusinessType)
admin.site.register(models.Address, admin.GeoModelAdmin)
admin.site.register(models.BusinessAccount)
admin.site.register(models.AdditionalDetails)
admin.site.register(models.SocialNetworks)
admin.site.register(models.Gallery)
admin.site.register(models.Reviews)
# admin.site.register()
# admin.site.register()
