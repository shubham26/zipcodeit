from django.contrib.gis.db import models
# from django.db import models
from django.db.models import Avg, Q, F
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _, gettext
from django.core.exceptions import ValidationError
from django.core import validators

from utils import models as util_model

SOCIAL_NETWORKS = (
    ('fa fa-facebook', _('Facebook')),
    ('fa fa-google-plus', _('Google +')),
    ('fa fa-twitter', _('Twitter')),
    ('fa fa-linkedin', _('LinkedIn')),
)

user_class = get_user_model()

DAY_OF_THE_WEEK = {
    1: _('Monday'),
    2: _('Tuesday'),
    3: _('Wednesday'),
    4: _('Thursday'),
    5: _('Friday'),
    6: _('Saturday'),
    7: _('Sunday'),
}

DAY_OF_WEEK_CHOICE = (
    (1, _('Monday')),
    (2, _('Tuesday')),
    (3, _('Wednesday')),
    (4, _('Thursday')),
    (5, _('Friday')),
    (6, _('Saturday')),
    (7, _('Sunday')),
)


class IndustryCategory(util_model.TimeStamp):
    category = models.CharField(_('Category'), max_length=100, unique=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Industry Category')
        verbose_name_plural = _('Industry Categories')

    def __str__(self):
        return self.category


class IndustryType(util_model.TimeStamp):
    category = models.ForeignKey('business_account.IndustryCategory', models.CASCADE, related_name='industry_type',
                                 verbose_name=_('Category'))
    type = models.CharField(_('Type'), max_length=100, )
    icon = models.ImageField(_('Icon'))

    class Meta:
        unique_together = ['category', 'type']
        ordering = ['-created_at']
        verbose_name = _('Industry Type')
        verbose_name_plural = _('Industry Types')

    def __str__(self):
        return "%s - %s" % (str(self.category), self.type)


class BusinessType(util_model.TimeStamp):
    type = models.CharField(_('Business Type'), max_length=30,  unique=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Business Type')
        verbose_name_plural = _('Business Types')

    def __str__(self):
        return self.type


class BusinessAccount(util_model.TimeStamp):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='business',
                             verbose_name=_('User'))
    position = models.CharField(_('Position'), max_length=50)
    business_name = models.CharField(_('Business Name'), unique=True, max_length=30)
    business_type = models.ForeignKey('business_account.BusinessType',
                                      on_delete=models.SET_NULL, null=True, verbose_name=_('Business Type'),)
    industry_type = models.ForeignKey('business_account.IndustryType',
                                      on_delete=models.SET_NULL, null=True, verbose_name=_('Industry Type'), )
    phone = models.BigIntegerField(_('Business Contact No.'),
                                             validators=[validators.MaxValueValidator(int('9'*12))])
    email = models.EmailField(_('Email'))
    address = models.ForeignKey('business_account.Address', on_delete=models.CASCADE, related_name='business')
    # slug_field =

    class Meta:
        ordering = ['industry_type', 'business_type', 'created_at']
        verbose_name = _('Business Account')
        verbose_name_plural = _('Business Accounts')

    def __str__(self):
        return " %s - %s - %s" % (str(self.industry_type), str(self.business_type), self.business_name,)

    @property
    def name(self):
        return self.business_name

    @property
    def rating(self):
        return self.business_reviews.aggregate(star_ratings=Avg('rating')).get('star_ratings')


class PaymentsDetails(util_model.TimeStamp):
    business = models.OneToOneField('business_account.BusinessAccount', on_delete=models.CASCADE,
                                    related_name='payment_details', verbose_name=_('Business'))
    card_number = models.BigIntegerField(_('Card Number'),
                                         validators=[validators.MaxValueValidator(int('9'*16)),
                                                     validators.MinValueValidator(0)])
    card_holder = models.CharField(_('Name On Card'), max_length=100)
    expiry = models.DateField(_('Expiry Date'))
    cvv = models.PositiveSmallIntegerField(_('cvv'), validators=[validators.MaxValueValidator(999)])
    billing_address = models.ForeignKey('business_account.Address', on_delete=models.CASCADE,
                                        related_name='billing_address', verbose_name=_('Billing Address'))
    digital_signature = models.CharField(_('Digital Signature'), max_length=50)

    class Meta:
        ordering = ['business']
        verbose_name = _('Payments Detail')
        verbose_name_plural = _('Payments Details')

    def __str__(self):
        return " payment details of account - %s " % (str(self.business))


class CostumerType(util_model.TimeStamp):
    business = models.OneToOneField('business_account.BusinessAccount', on_delete=models.CASCADE,
                                    related_name='consumer_type', verbose_name=_('Business'))
    demography = models.CharField(_('Demography'), max_length=50)
    age_range = models.CharField(_('Age Range'), max_length=10)  # todo, choices=()
    income_range = models.CharField(_('Income Range'), max_length=20)  # todo,  choices=()
    category_treading = models.CharField(_('Category Treading to'), max_length=50)
    unionized = models.CharField(_('Unionized'), max_length=50)
    customer_type = models.CharField(_('Customer Type'), max_length=50)

    class Meta:
        ordering = ['business']
        verbose_name = _('Costumer Type')
        verbose_name_plural = _('Costumer Types')

    def __str__(self):
        return " Costumer Type details of account - %s " % (str(self.business))


# todo class Product()

class AdditionalDetails(util_model.TimeStamp):
    business = models.OneToOneField('business_account.BusinessAccount', on_delete=models.CASCADE,
                                    related_name='additional_details', verbose_name=_('Business'))
    description = models.TextField(_('Description'))
    icon = models.ImageField(_('Icon'))
    #todo add fields and migrate
    # logo_icon = models.ImageField(_('Logo Icon'))
    additional_info = models.TextField(_('Additional Info'))
    other = models.TextField(_('Other'))
    buy_online = models.TextField(_('Buy Online Text'))
    credit_card = models.CharField(_('Credit Card'), max_length=30)
    cash = models.CharField(_('Cash'), max_length=30)

    class Meta:
        ordering = ['business']
        verbose_name = _('Additional Detail')
        verbose_name_plural = _('Additional Details')

    def __str__(self):
        return " Additional Details of account - %s " % (str(self.business))


class WorkingHour(models.Model):
    business = models.ForeignKey('business_account.BusinessAccount', on_delete=models.CASCADE,
                                 related_name='working_hours', verbose_name=_('Business'))
    day = models.PositiveSmallIntegerField(_('Day'), choices=DAY_OF_WEEK_CHOICE)
    opening_time = models.TimeField(_('Opening Time'), blank=True, null=True)
    closing_time = models.TimeField(_('Closing Time'), blank=True, null=True)

    class Meta:
        unique_together = ['business', 'day']
        ordering = ['business']
        verbose_name_plural = _('Working Hours of Businesses')

    def clean(self):
        if bool(self.opening_time) != bool(self.closing_time):
            raise ValidationError(gettext('Either set opening time and closing time both or not any one for close'))

    def __str__(self):
        working_hours = 'Closed'
        if self.opening_time and self.closing_time:
            working_hours = " %s - %s " % (self.opening_time, self.closing_time)
        return " %s - %s " % (DAY_OF_THE_WEEK.get(self.day), working_hours)


class Gallery(util_model.TimeStamp):
    business = models.ForeignKey('business_account.BusinessAccount', on_delete=models.CASCADE, related_name='gallery',
                                 verbose_name=_('Business'))
    image = models.ImageField(_('Image'))

    class Meta:
        ordering = ['created_at']
        verbose_name = _('Gallery Image')
        verbose_name_plural = _('Gallery Images')

    def __str__(self):
        return " Gallery Image of %s Business" % self.business.business_name


# class ReviewsManager()


class Reviews(util_model.TimeStamp):
    business = models.ForeignKey('business_account.BusinessAccount',  on_delete=models.CASCADE,
                                 related_name='business_reviews', verbose_name=_('Business'))
    user = models.ForeignKey(user_class, on_delete=models.CASCADE, related_name='user_reviews', verbose_name=_('User'))
    review = models.TextField(_('Review'))
    rating = models.PositiveSmallIntegerField(_('Rating'), validators=[validators.MaxValueValidator(5)])

    class Meta:
        unique_together = ['business', 'user']

    def __str__(self):
        return " %i star %s.... " % (self.rating, self.review[:10])

    def clean(self):
        if self.business.user == self.user:
            raise ValidationError(gettext("User is not allowed to give review on its own business"))


class Address(util_model.TimeStamp):
    user = models.ForeignKey('user_account.User', on_delete=models.CASCADE, related_name='address',
                             verbose_name=_('User'))
    address_1 = models.TextField(_('Address Line 1'))
    address_2 = models.TextField(_('Address Line 2'))
    state = models.CharField(_('State'), max_length=100)
    city = models.CharField(_('City'), max_length=100)
    zipcode = models.IntegerField(_('Zipcode'), validators=[validators.MaxValueValidator(int('9'*6)),
                                                            validators.MinValueValidator(int('9'*5)+1)])
    contact = models.BigIntegerField(_('Contact'), validators=[validators.MaxValueValidator(int('9'*12))])
    geo_loc = models.PointField(verbose_name=_('Geo Location'), srid=4326, null=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return " %s %s %s " % (self.state, self.city, self.zipcode)


class SavedBusiness(util_model.TimeStamp):
    user = models.OneToOneField(user_class, on_delete=models.CASCADE)
    favourites = models.ForeignKey('business_account.BusinessAccount', on_delete=models.CASCADE)

    class Meta:
        ordering = ['user', 'created_at']

    def __str__(self):
        return self.favourites.business_name


class SocialNetworks(util_model.TimeStamp):
    business = models.ForeignKey('business_account.BusinessAccount', on_delete=models.CASCADE, related_name='social',
                                 verbose_name=_('Business'))
    network = models.CharField(_('Social Network'), max_length=30, choices=SOCIAL_NETWORKS)
    url = models.URLField(_('Link of Account'))

    class Meta:
        verbose_name = _('Social Network')
        verbose_name_plural = _('Social Networks')
        unique_together = ['business', 'network']

    def __str__(self):
        return self.network[6:].capitalize()

    @property
    def name(self):
        return self.network[6:]
