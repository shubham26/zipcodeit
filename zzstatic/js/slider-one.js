(function ($) {
 "use strict";

/*--------------------------------
 featured item
---------------------------------- */
     $(".featured-item").owlCarousel({
      navigation : true,
      pagination : false,
      slideSpeed : 600,
	  autoPlay : false,
      paginationSpeed : 400,
      items :4,
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [979,4], 
      itemsTablet: [767,1], 
      itemsMobile : [480,1],
      navigationText : ['<i class="icon-left-open"><i class="fa fa-angle-left"></i></i>','<i class="icon-right-open"><i class="fa fa-angle-right"></i></i>'] 
  }); 
	
})(jQuery);    


/*--------------------------------
 slide product
---------------------------------- */   
     $(".slide-product").owlCarousel({
      navigation : true, 
      pagination : false,
      slideSpeed : 600,
      paginationSpeed : 400,
      items : 2,
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [979,2], 
      itemsTablet: [767,2], 
      itemsMobile : [480,1],
      navigationText : ['<i class="icon-left-open"><i class="fa fa-angle-left"></i></i>','<i class="icon-right-open"><i class="fa fa-angle-right"></i></i>'] 
  });

