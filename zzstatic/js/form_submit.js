$(document).ready(() => {
        $.AjaxHandler({
            submitBtn: '#login-bt',
            form_selector: '#login-form',
            postSuccess: function (form, response, instance) {
                $(instance.options.submitBtn).button('reset');
                form.reset();
            },
            onSuccess: function (form, response) {
                Snackbar.show({pos: 'top-center', actionTextColor: '#00ff00', text: response['message']});
                $(form).hide('slow');
                $('.contact-success').show().text(response['message'])
            },
        });
        $.AjaxHandler({
            submitBtn: '#signup-bt',
            form_selector: '#signup-form',
            postSuccess: function (form, response, instance) {
                $(instance.options.submitBtn).button('reset');
                form.reset();
            },
            onSuccess: function (form, response) {
                Snackbar.show({pos: 'top-center', actionTextColor: '#00ff00', text: response['message']});
                $(form).hide('slow');
                $('.contact-success').show().text(response['message'])
            },
        });
});

