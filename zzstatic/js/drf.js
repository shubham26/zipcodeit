function getCookie(name) {
    var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                let cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    let csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});


function notifyServerError() {
    Snackbar.show({pos: 'top-center', actionTextColor: '#00ff00', text: gettext('Error occurred. Refresh the page and try again.')});
}
(function ($, window, document, undefined) {
    let pluginName = 'AjaxHandler';
    let defaults = {
	    container: $(document),
	    formSets: '',
	    formSetItem: 'tbody tr',
	    captchaField: '#div_id_captcha',
	    removeErrors: function(form, instance){
	        $(form).find('.has-error').each(function (item) {
	           $('div.help-block', item).remove();
	        }).removeClass('has-error');
	    },
            processErrors: null,
            afterResponse: null,
	    beforeSubmission: function(form, instance){
	        $(instance.options.submitBtn).button('loading');
	        return true;
	    },
	    onSuccess: function(form, response){
	        Snackbar.show({pos: 'top-center', actionTextColor: '#00ff00', text: response['message']});
	        if (response['redirect_url']) {
	            setTimeout(function() {window.location.href = response['redirect_url']}, 1500)
	        }
	    },
	    postSuccess: function(form, response, instance) {
	        $(instance.options.submitBtn).button('reset').hide('slow');
	        $(form).hide('slow')
	    }
	};

    $.fn.attrs = function() {
        if (arguments.length === 0) {
            if (this.length === 0) {
                return null;
            }

            let obj = {};
            $.each(this[0].attributes, function() {
                if (this.specified) {
                    obj[this.name] = this.value;
                }
            });
            return obj;
        }
    };

    function Plugin(element, options) {
        this.options = $.extend( {}, defaults, options) ;
        this._defaults = defaults;
        this.form = element;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype.init = function () {

        let instance = this;
        let performSubmit = function (e) {
            e.preventDefault();
            instance.form = e.target.form;
            instance.options.removeErrors(instance.form, instance);
            let status = instance.options.beforeSubmission(instance.form, instance);
            if (status)
                submit();
        };

        let submit = function() {
            $.ajax({
                url: $(instance.form).attr('action'),
                type: 'post',
                data: new FormData(instance.form),
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function (payload) {
                if(typeof instance.options.onSuccess === 'function')
                    instance.options.onSuccess(instance.form, payload);

                if(typeof instance.options.postSuccess === 'function')
                    instance.options.postSuccess(instance.form, payload, instance);
            })
            .fail(function(response) {
                if(typeof instance.options.processErrors === 'function')
                    instance.options.processErrors(instance.form, response);
                else
                    processErrors(response);
                if(typeof instance.options.afterResponse === 'function')
                    instance.options.afterResponse(instance.form, response);
            })
        };

        let resetBtn = function() {
            $(instance.options.submitBtn).button('reset');
        };

        let processErrors = function(response) {
            // we take the JSON of response and further format it for toaster
            let data = JSON.parse(response.responseText);

            if (response.status === 400 && data['errors']) {
                $.map( data['errors'], function( v, k ) {
                    if(k === 'error_message'){
                    }else{
                        if(k === '__all__') {
                            // toastr.error(v);
                        }
                        else
                        {
                            let field = $(instance.form).find('[name='+k+']');
                            field.parents('.form-group').addClass('has-error');
                            if(field.parent().next() && field.parent().next().hasClass('help-block')){
                                field.parent().next().text(v);
                            }else{
                                field.parent().after('<div class="help-block">'+v+'</div>');
                            }
                            if(k === "captcha"){
                                let field = $(instance.form).find(instance.options.captchaField);
                                $(field).addClass('has-error');
                            }
                        }
                    }
                }).join('<br>');

            }else{
                notifyServerError()
            }
            resetBtn();
        };

        instance.options.container.on('click', instance.options.submitBtn, performSubmit);
        console.log(instance.options.container, "Container")
    };

    $[pluginName] = function ( options ) {
        return [$.data($(options.form_selector), 'plugin_' + pluginName, new Plugin( this, options ))]
    }
})( jQuery, window, document );