django==2.1
social-auth-app-django==3.1.0
django-recaptcha==2.0.2
psycopg2-binary==2.7.7
geoip2==2.9.0
