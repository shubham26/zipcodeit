
from zipcodeit.settings.base import *

DEBUG = False


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = ''  # client id
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = ''  # client secret


SOCIAL_AUTH_FACEBOOK_KEY = ''
SOCIAL_AUTH_FACEBOOK_SECRET = ''


RECAPTCHA_PUBLIC_KEY = ''
RECAPTCHA_PRIVATE_KEY = ''
NOCAPTCHA = True
# RECAPTCHA_DOMAIN = 'www.recaptcha.net'


# specify email settings
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

STATIC_ROOT = join(PROJECT_ROOT, 'zzstatic')

GOOGLE_MAP_API = None