from django import template

register = template.Library()


@register.filter
def form_fields(form):
    return list(form.fields.keys())


# @register.simple_tag
# def tag_test(val):
#     return val+str('_aman')
